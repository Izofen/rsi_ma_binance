import ccxt
import json
import talib  
import numpy

##  KLIENT
apiKey = "*****************************************"
secret = "*******************************************"
client = ccxt.binance ({'apiKey': apiKey,'secret': secret})
data = client.fetchMarkets()
#print ("[+]",answer)
for line in data:
    pair = line['symbol']	
    orderbook = client.fetch_order_book (pair)
    ask = orderbook['asks'][0][0] if len (orderbook['asks']) > 0 else None
    bid = orderbook['bids'][0][0] if len (orderbook['bids']) > 0 else None    
   
    if ask != None and bid != None:
        ask_s = str('{:10.20f}'.format(ask))
        bid_s = str('{:10.20f}'.format(bid))
        timeframes = '1h'  
        data = client.fetchOHLCV (pair,timeframes,limit = 2)
        close = 0
        for line in data:
            OHLCV = line
            close = close + line[4]
            #print ('    [+]',line)
        ma2 = close / 2 
        pr_ask = ma2 * 100 / ask
        
        list = numpy.zeros(15) 
        data = client.fetchOHLCV (pair,timeframes,limit=15)
        
        nm = 0
        for line in data:
            list[nm] = line[4]
            nm = nm +1
            #print ('[+] list:',list) 
            output = talib.RSI(list,timeperiod=14)
            #print (output)
        print ('[+] {:20s}  {}  {}  MA2: {} {:} {}'.format (pair,ask,bid,ma2,pr_ask,output[14]))

